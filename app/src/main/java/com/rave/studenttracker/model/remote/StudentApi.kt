package com.rave.studenttracker.model.remote

import com.rave.studenttracker.model.dto.StudentDTO

/**
 * Student api.
 *
 * @constructor Create empty Student api
 */
interface StudentApi {
    /**
     * Fetch student list from server.
     *
     * @return [List] of [StudentDTO]
     */
    suspend fun fetchStudentList(): List<StudentDTO>
}
