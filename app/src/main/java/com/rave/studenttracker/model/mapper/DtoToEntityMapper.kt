package com.rave.studenttracker.model.mapper

/**
 * Dto to entity mapper.
 *
 * @param DTO
 * @param ENTITY
 * @constructor Create empty Dto to entity mapper
 */
interface DtoToEntityMapper<in DTO, out ENTITY> {
    /**
     * Invoke.
     *
     * @param dto
     * @return
     */
    operator fun invoke(dto: DTO): ENTITY
}
