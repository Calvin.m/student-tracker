package com.rave.studenttracker.model

import com.rave.studenttracker.model.entity.Student
import com.rave.studenttracker.model.mapper.student.StudentMapper
import com.rave.studenttracker.model.remote.StudentApi
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

/**
 * Student repo.
 *
 * @property studentApi
 * @property studentMapper
 * @property dispatcher
 * @constructor Create empty Student repo
 */
class StudentRepo(
    private val studentApi: StudentApi,
    private val studentMapper: StudentMapper,
    private val dispatcher: CoroutineDispatcher = Dispatchers.IO
) {

    /**
     * Fetches [List] of [StudentDTO] from server and maps to [List] of [Student].
     *
     * @return
     */
    suspend fun getStudentList(): List<Student> {
        return withContext(dispatcher) {
            studentApi.fetchStudentList().map {
                studentMapper.invoke(it)
            }
        }
    }
}
