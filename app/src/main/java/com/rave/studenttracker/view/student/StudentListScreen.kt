package com.rave.studenttracker.view.student

import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import coil.compose.AsyncImage
import com.rave.studenttracker.model.entity.Student

@Composable
fun StudentListScreen(students: List<Student>) {
    LazyColumn {
        items(
            items = students,
            key = { student: Student -> student.id }
        ) { student: Student ->
            StudentCard(student = student)
        }
    }
}

@Composable
fun StudentCard(student: Student) {
    Card(
        colors = CardDefaults.cardColors(
            containerColor = MaterialTheme.colorScheme.tertiary
        ),
        modifier = Modifier.padding(12.dp)
    ) {
        AsyncImage(
            model = student.avatar,
            contentDescription = null,
            modifier = Modifier.size(50.dp, 50.dp)
        )
        Text(
            text = "${student.firstName} ${student.lastName}\n${student.email}\n${student.university}",
            modifier = Modifier.padding(12.dp)
        )
    }
}
