package com.rave.studenttracker.view.student

import com.rave.studenttracker.model.entity.Student

/**
 * Student list state [StudentListScreen].
 *
 * @property isLoading is true if fetching new student data, otherwise false
 * @property students represents [List] of [Student]
 * @constructor Create new instance of [StudentListState]
 */
data class StudentListState(
    val isLoading: Boolean = false,
    val students: List<Student> = emptyList()
)
