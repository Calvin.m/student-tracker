package com.rave.studenttracker.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import com.rave.studenttracker.model.StudentRepo
import com.rave.studenttracker.view.student.StudentListState
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch

/**
 * This [ViewModel] holds all student related data.
 *
 * @property repo Repository to fetch Student data [StudentRepo]
 * @constructor Create new instance of [StudentListViewModel]
 */
class StudentListViewModel(private val repo: StudentRepo) : ViewModel() {

    private val _studentListState = MutableStateFlow(StudentListState())
    val studentListState: StateFlow<StudentListState> get() = _studentListState

    /**
     * This method fetches a list of students using [StudentRepo] and updates the [StudentListState].
     *
     */
    fun fetchStudents() {
        _studentListState.update { state -> state.copy(isLoading = true) }
        viewModelScope.launch {
            val students = repo.getStudentList()
            _studentListState.update { state -> state.copy(isLoading = false, students = students) }
        }
    }

    companion object {
        /**
         * New instance.
         *
         * @param repo
         */
        fun newInstance(repo: StudentRepo) = object : ViewModelProvider.Factory {
            override fun <T : ViewModel> create(modelClass: Class<T>): T {
                return StudentListViewModel(repo) as T
            }
        }
    }
}
