package viewmodel

import com.rave.studenttracker.model.StudentRepo
import com.rave.studenttracker.model.dto.StudentDTO
import com.rave.studenttracker.model.entity.Student
import com.rave.studenttracker.model.mapper.student.StudentMapper
import com.rave.studenttracker.model.remote.StudentApi
import com.rave.studenttracker.model.testUtil.CoroutinesTestExtension
import com.rave.studenttracker.view.student.StudentListState
import com.rave.studenttracker.viewmodel.StudentListViewModel
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.launch
import kotlinx.coroutines.test.runTest
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.RegisterExtension

@OptIn(ExperimentalCoroutinesApi::class)
class StudentListViewModelTest {

    @RegisterExtension
    private val coroutinesTestExtension = CoroutinesTestExtension()

    private val api = mockk<StudentApi>()
    private val studentMapper = StudentMapper()
    private val repo = StudentRepo(
        studentApi = api,
        studentMapper = studentMapper,
        dispatcher = coroutinesTestExtension.dispatcher
    )

    @Test
    @DisplayName("Testing that student list is fetched on creation of ViewModel")
    fun testFetchOnCreation() {
        val students = listOf(
            Student(
                avatar = "test",
                firstName = "teset",
                lastName = "test",
                id = 0,
                university = "test",
                email = "test"
            )
        )
// LAUNCH - acts like a while loop, need to cancel at end to keep from running forever
        coEvery { repo.getStudentList() } coAnswers { students }

        val studentListViewModel = StudentListViewModel(repo)
        val state = studentListViewModel.studentListState.value

        Assertions.assertTrue(state.students.isNotEmpty())
        Assertions.assertFalse(state.isLoading)
        Assertions.assertEquals(students, state.students)
    }

    @Test
    @DisplayName("Testing that student state starts off loading")
    fun testLoadingFetchOnCreation() = runTest(coroutinesTestExtension.dispatcher) {
        val dtos = listOf(
            StudentDTO(
                avatar = "test",
                firstName = "teset",
                lastName = "test",
                id = 0,
                university = "test",
                email = "test"
            )
        )
// LAUNCH - acts like a while loop, need to cancel at end to keep from running forever
        coEvery { api.fetchStudentList() } coAnswers { dtos }
        val stateUpdates = mutableListOf<StudentListState>()

        val studentListViewModel = StudentListViewModel(repo)
        val job = launch { studentListViewModel.studentListState.toList(stateUpdates) }
        studentListViewModel.fetchStudents()

        val (initState, loadingState, successState) = stateUpdates
        stateUpdates.onEachIndexed { index, state -> println("STATE #${index.plus(1)} ->" + state) }
        val students = dtos.map { dto -> studentMapper(dto) }

        Assertions.assertFalse(initState.isLoading)
        Assertions.assertTrue(initState.students.isEmpty())

        Assertions.assertTrue(loadingState.isLoading)
        Assertions.assertTrue(loadingState.students.isEmpty())

        Assertions.assertTrue(successState.students.isNotEmpty())
        Assertions.assertFalse(successState.isLoading)
        Assertions.assertEquals(students, successState.students)

        job.cancel()
    }
}
