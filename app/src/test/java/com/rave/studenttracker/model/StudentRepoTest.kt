package com.rave.studenttracker.model

import com.rave.studenttracker.model.dto.StudentDTO
import com.rave.studenttracker.model.entity.Student
import com.rave.studenttracker.model.mapper.student.StudentMapper
import com.rave.studenttracker.model.remote.StudentApi
import com.rave.studenttracker.model.testUtil.CoroutinesTestExtension
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.test.runTest
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

internal class StudentRepoTest {
    private val coroutinesTestExtension = CoroutinesTestExtension()

    private val api = mockk<StudentApi>()
    private val studentMapper = StudentMapper()
    private val repo = StudentRepo(studentApi = api, studentMapper = studentMapper)

    @Test
    fun testGetStudentsList() = runTest(coroutinesTestExtension.dispatcher) {
        // Given
        val studentDtos = listOf(
            StudentDTO(
                avatar = "test",
                firstName = "teset",
                lastName = "test",
                id = 0,
                university = "test",
                email = "test"
            )
        )
        coEvery { api.fetchStudentList() } coAnswers { studentDtos }
        // When
        val studentList: List<Student> = repo.getStudentList()
        // Then
        val students = studentDtos.map { dto -> studentMapper(dto) }
        assertEquals(students, studentList)
    }
}
